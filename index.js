const express = require('express')
const config = require('config')
const mongoose = require('mongoose')

const app = express()
app.use(express.json({extended:true}))

app.use('/api/auth',require('./routes/auth.routes'))
app.use('/api/problem',require('./routes/problems.routes'))
app.use('/api/alt',require('./routes/alternative.routes'))
app.use('/api/experts',require('./routes/experts.routes'))
app.use('/api/result',require('./routes/result.routes'))
const PORT = config.get('port') || 5000

async function start(){
    try{
        await mongoose.connect(config.get('mongoUri'),{
            useCreateIndex:true,
            useNewUrlParser:true,
            useUnifiedTopology:true
        })
        app.listen(PORT, ()=> console.log("App started on port ", PORT))
    }
    catch(e){
        console.log("Server error! ", e.message)
    }
}

start()