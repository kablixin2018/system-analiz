import React, { useEffect, useState, useContext} from 'react'
import {NavLink } from 'react-router-dom'
import {useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'
import '../style/AddPage.css'

export const AddPage = () =>{
    const auth = useContext(AuthContext)
    const message = useMessage()
    const {loading,error,request,clearError}=useHttp()
    const [form,setForm]=useState({
      title:'',main:'',username:auth.userName,value:''
    })
  
    useEffect(()=>{
      message(error)
      clearError()  
    },[error,message,clearError])

    const changeHandler = event =>{
        setForm({ ...form, [event.target.name]:event.target.value })
    }
    const sendHandler = async () =>{
        try {
            console.log('Form', {...form})
            const data = await request('/api/problem/add','POST',{...form})
            message(data.message)
            window.location.reload()
        } catch (e) {}
    }
    return(
        <div className="container">
            <div className="row">
                <div className="col-md"><h1>Добавить проблему</h1></div>
            </div>
            <div className="row">
                <form>
                    <div className="form-group">
                            <p>Заголовок проблемы</p>
                            <input className="title" type="text" name="title" id="title" onChange={changeHandler}></input>
                    </div>
                    <div className="form-group">
                            <p>Формулировка проблемы</p>
                            <textarea className="main_area" type="text" name="main" id="main" onChange={changeHandler}></textarea>
                    </div>
                    <div className="form-group">
                            <p>Укажите шкалу(для метода полных попарных сопоставлений)</p>
                            <input type="text" name="value" id="value" onChange={changeHandler}></input>
                    </div>
                    <div className="form-group">
                        <button type="button" className="btn btn-default" disabled={loading} onClick={sendHandler}>Отправить</button>
                    </div>
                </form>
            </div>
        </div>
    )
}