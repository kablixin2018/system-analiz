import React, {useCallback, useEffect, useState, useContext} from 'react'
import {useParams} from 'react-router-dom'
import {useHttp, } from '../../hooks/http.hook'
import { useMessage } from '../../hooks/message.hook'
import {AuthContext} from '../../context/AuthContex'
import{FmpCardExp} from './Components/FpmCardExp'
import {Loader} from '../../Components/Loader'

export const FPMPage = () =>{
    const {token} = useContext(AuthContext)
    const {request, loading} = useHttp()
    const [problem, setProblem] = useState([])
    const [alt,setAlt] = useState([])
    const title = useParams().title

    const getProblem = useCallback(async () =>{
        try {
            console.log(title)
            const fetched = await request('/api/problem/getproblem','PUT',{title})
            console.log(fetched)
            setProblem(fetched)
        } catch (e) {
            
        }
    },[token,title,request])

    const fetchproblem = useCallback(async ()=>{
        try {
           const fetch = await request('/api/alt/getalt','POST',{title})
           setAlt(fetch)
        } catch (e) {}
    },[token,request])

    useEffect(()=>{
        getProblem()
    },[getProblem])

    useEffect(()=>{
        fetchproblem()
    },[fetchproblem])

    if(loading){
        return <Loader/>
    }
    return(
        <>
            {!loading && title && <FmpCardExp problem={problem} title={title} alt={alt}/>}
        </>
    )
    
}