import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useParams} from 'react-router-dom'
import {useHttp} from '../hooks/http.hook'
import {AuthContext} from '../context/AuthContex'
import{ProblemCard} from '../Components/ProblemCard'
import {Loade, Loader} from '../Components/Loader'

export const ProblemViewPage = () =>{

    const {token} = useContext(AuthContext)
    const {request, loading} = useHttp()
    const [problem, setProblem] = useState([])
    const [alt,setAlt] = useState([])
    const [experts,setExperts] = useState([])
    const title = useParams().title

    const getProblem = useCallback(async () =>{
        try {
            console.log(title)
            const fetched = await request('/api/problem/getproblem','PUT',{title})
            setProblem(fetched)
        } catch (e) {
            
        }
    },[token,title,request])

    const fetchproblem = useCallback(async ()=>{
        try {
           const fetch = await request('/api/alt/getalt','POST',{title})
           setAlt(fetch)
        } catch (e) {}
    },[token,request])
    
    const fetchexp = useCallback(async ()=>{
        try {
           const fetch = await request('/api/experts/getExperts','POST',{title})
           setExperts(fetch)
        } catch (e) {}
    },[token,request])

    useEffect(()=>{
        getProblem()
    },[getProblem])

    useEffect(()=>{
        fetchproblem()
    },[fetchproblem])
    useEffect(()=>{
        fetchexp()
    },[fetchexp])

    if(loading){
        return <Loader/>
    }
    return(
        <>
            {!loading && title && <ProblemCard alt={alt} problem={problem} title={title} experts={experts}/>}
        </>
    )
}