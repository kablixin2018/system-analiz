const{Schema,model,Types} = require("mongoose")

const schema = new Schema({
    title:{type:String,require:true, unique:true},
    username:{type:String,require:true,unique:false},
    result:[{
        name:{type:String,require:true,unique:false},
        value:{type:Number,require:true,unique:false}
    }]
})

module.exports = model('Pref', schema)