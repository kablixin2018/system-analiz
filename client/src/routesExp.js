import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import {AuthPage} from './Pages/AuthPage'
import{RegisterPage} from './Pages/RegisterPage'
import {MainExpertPage} from './Pages/Expert/MainExpertPage'
import {ProblemViewPage}  from './Pages/Expert/ProblemViewPage'
import{AddInformPage} from './Pages/Expert/AddInformPage'
import{ RangPage} from './Pages/Expert/RangPage'
import{ PreferencePage} from './Pages/Expert/PreferencePage'
import{ FPMPage} from './Pages/Expert/FPMPage'
import{ EjPage} from './Pages/Expert/EjPage'
import{ PCPage} from './Pages/Expert/PCPage'


export const useRoutesExp = (isAuthenticated,) => {
    if(isAuthenticated){
        return(
            <Switch>
                <Route path="/index" >
                    <MainExpertPage/>
                </Route>
                <Route path="/addInfo" >
                    <AddInformPage/>
                </Route>
                <Route path="/problemView/:title">
                    <ProblemViewPage/>
                </Route>
                <Route path="/fpm/:title" >
                    <FPMPage/>
                </Route>
                <Route path="/rang/:title" >
                    <RangPage/>
                </Route>
                <Route path="/pref/:title" >
                    <PreferencePage/>
                </Route>
                <Route path="/ej/:title" >
                    <EjPage/>
                </Route>
                <Route path="/pc/:title" >
                    <PCPage/>
                </Route>

                <Redirect to="index" />
            </Switch>
        )
       
    }
    return(
        <Switch>
            <Route path="/" exact>
                <AuthPage/>
            </Route>
            <Route path="/registration" exact>
                <RegisterPage/>
            </Route>
            <Redirect to="/" />
        </Switch>

    )
}