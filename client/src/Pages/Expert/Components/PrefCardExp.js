import React, {useCallback, useEffect, useState, useContext} from 'react'
import {Link} from 'react-router-dom'
import {useHttp, } from '../../../hooks/http.hook'
import { useMessage } from '../../../hooks/message.hook'
import {AuthContext} from '../../../context/AuthContex'

export const PrefCardExp = ({problem,title,alt}) =>{
    const [form,setForm]=useState({
        
      })
    const {loading,error,request,clearError}=useHttp()
    const message = useMessage()
    const {userName} = useContext(AuthContext)
    useEffect(()=>{
        message(error)
        clearError()  
      },[error,message,clearError])

    const changeHandler = event =>{
        setForm({ ...form, [event.target.name]:event.target.value })
    }

    const registerHandler = async () =>{
        const data_send = {
            alt:{...form},
            userName: userName,
            title:problem.title
        }
        console.log({...form})
        try {
            const data = await request('/api/result/pref',"POST",data_send)
            message(data.message)
            window.location.reload()
        } catch (e) {}
      } 
    return(
       <div className="container">
           <div className="row">
               <h1>{problem.title}</h1>
               <p>Дайте оценку каждой альтернативе, записав числа в поле напротив. Расставьте числа в порядке убывания
                   важности альтернатив, т.е 1 - для наиболее важной альтернативы, 2 - менее и т.д. Числа не должны повторяться.
               </p>
           </div>
           <div className="row">
            {alt.map(alt=>{
                return(
                    <div className="col-md-12 problem_container" >
                        <form>
                            <div className="header_problem">
                                <span className="ej_span">{alt.text}</span>
                                <span><input size="4" type="text" id={alt.text} name={alt.text} placeholder="Оценка" onChange={changeHandler}/></span>
                            </div>
                            
                        </form>
                        </div>
                    )
            })}
            <div className="form-group">
                <button type="submit" className="btn btn-default" disabled={loading} onClick={registerHandler}>Отправить</button>
             </div>
          </div>
       </div>
    )
        
}