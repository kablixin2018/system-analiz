import React, {useCallback, useEffect, useState, useContext} from 'react'
import {Link} from 'react-router-dom'
import '../style/ProblemCard.css'
import {useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'

export const ProblemCardExp = ({problem,alt,title}) =>{
    const auth = useContext(AuthContext)
    const {token,userName} = useContext(AuthContext)
    const message = useMessage()
    const {loading,error,request,clearError}=useHttp()
    const [form,setForm]=useState({
      titleProblem:title,text:''
    })
    const [form2,setForm2]=useState({
        titleProblem:title, _id:''
      })

    const [methods,setMethod] = useState([])

    
    
    useEffect(()=>{
      message(error)
      clearError()  
    },[error,message,clearError])
    
    const check_send_dec = ()=>{
        if(!methods.pc||!methods.ej||!methods.fpm||!methods.rang||!methods.pref)
            return false
        else
            return true
    }
    const sendDec = useCallback(async ()=>{
        const check = check_send_dec()
        console.log(check)
        let data_fetch={
            title:title,
            userName:userName
        }
        if(check){
            const data = await request('/api/problem/dec_send_problem','POST',data_fetch)
            const data_message = "Успешно"
            message(JSON.stringify(data_message))
            window.location.reload()
            
        }
        else{
            const data = "Выполните все методы!"
            message(JSON.stringify(data))
        }
    })

    const fetchMethod = useCallback(async ()=>{
        try {
            const send_data = {title:title,userName:userName}

            const fetch = await request('/api/result/check_dec','POST',send_data)
            setMethod(fetch)
        } catch (e) {
            
        }
    },[token,request])
    useEffect(()=>{
        fetchMethod()
    },[fetchMethod])
    console.log("sdada",{...methods})
    return(
        <div className="container all_problem_container">
            <div className="row">
                <div className="problem_header">
                    <h1>{problem.title}</h1>
                </div>
            </div>
            <div className="row">
                <div className="problem_main">
                    <p>{problem.main}</p>
                </div>
            </div>
            <div className="row">
                <h2>Список альтернатив</h2>
                
                {alt.map(alt=>{
                            return(
                                <div className="col-md-12 problem_container" >
                                    <div className="header_problem">
                                        <span>{alt.text}</span>
                                    </div>
                                </div>
                            )
                        })}
            </div>
            <div className="row">
                <h2>Методы решения<br/></h2>
            </div>
            <div className="row">
                    <Link className="btn btn-primary" to={`/pc/${problem.title}`}>Метод парных сравнений
                    </Link>
                    {methods.pc ? <span>Отправлено</span>: <span>Не отправлено</span>}
            </div>
            <div className="row">
                <Link className="btn btn-primary" to={`/ej/${problem.title}`}>Метод взвешенных экспертных оценок
                </Link><br/>
                {methods.ej ? <span>Отправлено</span>: <span>Не отправлено</span>}
            </div>
            <div className="row">
                <Link className="btn btn-primary" to={`/pref/${problem.title}`}>Метод предпочтений
                </Link>
                {methods.pref ? <span>Отправлено</span>: <span>Не отправлено</span>}
            </div>
            <div className="row">
                <Link className="btn btn-primary" to={`/rang/${problem.title}`}>Метод ранга
                </Link>
                {methods.rang ? <span>Отправлено</span>: <span>Не отправлено</span>}
            </div>
               
            <div className="row">
                <Link className="btn btn-primary" to={`/fpm/${problem.title}`}>Метод полного попарного сопоставления
                </Link>
                {methods.fpm ? <span>Отправлено</span>: <span>Не отправлено</span>}
                <br/>
            </div>
            <div className="row">
                <div className="col-md text-right">
                    <button className="btn btn-primary" onClick={sendDec}>Закончить оценивание</button>
                </div>
            </div>
        </div>
        )
}
