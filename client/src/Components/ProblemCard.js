import React, {useCallback, useEffect, useState, useContext} from 'react'
import {Link} from 'react-router-dom'
import '../style/ProblemCard.css'
import {useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'

export const ProblemCard = ({problem,alt,title,experts}) =>{
    const auth = useContext(AuthContext)
    const {token,userName} = useContext(AuthContext)
    const message = useMessage()
    const {loading,error,request,clearError}=useHttp()
    const [form,setForm]=useState({
      titleProblem:title,text:''
    })
    const [form2,setForm2]=useState({
        titleProblem:title, _id:''
      })

    useEffect(()=>{
      message(error)
      clearError()  
    },[error,message,clearError])
  
    const changeHandler = event =>{
      setForm({ ...form, [event.target.name]:event.target.value })
    }
    const addHandler = async () =>{
        try {
          console.log('Form', {...form})
          const data = await request('/api/alt/addAlt','POST',{...form})
          message(data.message)
          window.location.reload()
        } catch (e) {}
    }
    const deleteFunction = (_id) =>{
        form2._id = _id
        changesHandler()
        window.location.reload()
    } 
    const deleteFunction2 = (_id) =>{
        form2._id = _id
        deleteExp()
        window.location.reload()
    } 
    const changesHandler = async ()=>{
        try {
            const data = await request('/api/alt/delete','POST',{...form2})
            message(data.message)
            window.location.reload()
        } catch (e) {
            
        }
    }
    const deleteExp = async () =>{
        try {
            const data = await request('/api/experts/delete','POST',{...form2})
            message(data.message)
            window.location.reload()
        } catch (error) {
            
        }
    }
    console.log(problem.status)
    if(problem.status =="send"){
        return(
            <div className="container all_problem_container">
                <div className="row">
                    <div className="problem_header">
                        <h1>{problem.title}</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="problem_main">
                        <p>{problem.main}</p>
                    </div>
                </div>
                <div className="row">
                    <h2>Список альтернатив</h2>
                    
                    {alt.map(alt=>{
                                return(
                                    <div className="col-md-12 problem_container" >
                                        <div className="header_problem">
                                            <span>{alt.text}</span>
                    
                                        </div>
                                    </div>
                                )
                            })}
                </div>
                <div className="row">
                    <h2>Список выбранных экспертов</h2>
                    {experts.map(exp=>{
                                return(
                                    <div className="col-md-12 problem_container" >
                                        <div className="header_problem">
                                            <span>{exp.username}</span><br/>
                                            <span> Вес {exp.weight}</span><br/>
                                        </div>
                                    </div>
                                )
                            })}
                </div>
            </div>
        )
    }
    else{
        return(
            <div className="container all_problem_container">
                <div className="row">
                    <div className="problem_header">
                        <h1>{problem.title}</h1>
                    </div>
                </div>
                <div className="row">
                    <div className="problem_main">
                        <p>{problem.main}</p>
                    </div>
                </div>
                <div className="row">
                    <h2>Список альтернатив</h2>
                    
                    {alt.map(alt=>{
                                return(
                                    <div className="col-md-12 problem_container" >
                                        <div className="header_problem">
                                            <span>{alt.text}</span>
                                            <button className="btn btn-primary" onClick={()=>deleteFunction(alt._id)}>Удалить альтернативу</button>
                                        </div>
                                    </div>
                                )
                            })}
                </div>
                <div className="row">
                    <div className="col-md">
                        <h2>Добавить альтернативу</h2>
                        <form>
                            <div className="form-group">
                            <input
                                type="email"
                                className="form-control" 
                                id="titleProblem" 
                                name="text"
                                placeholder="Название"
                                onChange={changeHandler}
                                />
                                <i className="fa fa-user"></i> 
                            </div>
                            <div className="form-group">
                                <button type="submit" className="btn btn-default" disabled={loading} onClick={addHandler}>Добавить</button>
                            </div>
                        </form>
                    </div>
                </div>
                <div className="row">
                    <h2>Список выбранных экспертов</h2>
                    
                    {experts.map(exp=>{
                                return(
                                    <div className="col-md-12 problem_container" >
                                        <div className="header_problem">
                                            <span>{exp.username}</span><br/>
                                            <span> Вес {exp.weight}</span><br/>
                                            <button className="btn btn-primary" onClick={()=>deleteFunction2(exp._id)}>Удалить эксперта</button>
                                        </div>
                                    </div>
                                )
                            })}
                    <Link className="btn btn-primary" to={`/addExperts/${problem.title}`}>Добавить эксперта
                    </Link>
                </div>
            </div>
        )
    }
}
