const{Schema,model,Types} = require("mongoose")

const schema = new Schema({
    email:{type:String,require:true, unique:true},
    password:{type:String,require:true,unique:false},
    role:{type:String,require:true,unique:false},
    username:{type:String,require:true,unique:false},
    nameWork:{type:String,require:false,unique:false},
    position:{type:String,require:false,unique:false},
    age:{type:Number,require:false,unique:false}
})

module.exports = model('User', schema)