import React, { useCallback, useEffect, useState, useContext } from 'react'
import { useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import { AuthContext } from '../context/AuthContex'
import { ListDecProblem } from '../Components/ListDecProblem'
import { Loader } from '../Components/Loader'
import './DecCard.css'
export const DecProblemCard = ({ problem, title }) => {
    const auth = useContext(AuthContext)
    const { token, userName } = useContext(AuthContext)
    const message = useMessage()
    const { loading, error, request, clearError } = useHttp()

    if (!problem) {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-md">
                        <span>Пусто</span>
                    </div>
                </div>
            </div>
        )
    }
    console.log("Problem", problem)
    let i=1
    return (
        <div className="container Dec">
            <div className="row">
                <h1>{title}</h1>
            </div>
            <div className="col-md-12">
                <h3>Результаты оценивания</h3>
            </div>
            <div className="row">
                <div className="col-md-6 tex-center card">
                    <span id="title_method">Метод сравнения</span>
                    {problem.pc_result.map(res=>{

                        i+=1
                        return(
                            <div className="col-md-12 card">
                                <span>Эксперт {i-1}</span>
                                {res.map(result=>{
                                    return(
                                        <div className="col-md-12 card_item">
                                            <span  id="title">{result.name}</span>
                                            <span  id="value">{result.value}</span>
                                        </div>
                                    )
                                })}
                                
                            </div>
                            
                        )
                        
                    })}
                </div>
            </div>
            <div className="row">
                <div className="col-md-4 card">
                    <span id="title_method">Метод взвешенных экспертных оценок</span>
                    {problem.ej_result.map(res => {
                        return (
                            <div className="col-md-12 card_item">
                                <span  id="title">{res.name}</span>
                                <span  id="value">{res.value}</span>
                            </div>
                        )
                    })}
                </div>
                <div className="col-md-4 card">
                    <span id="title_method">Метод предпочтений</span>
                    {problem.pref_result.map(res => {
                        return (
                            <div className="col-md-12 card_item">
                                <span  id="title">{res.name}</span>
                                <span  id="value">{res.value}</span>
                            </div>
                        )
                    })}

                </div>
                <div className="col-md-4 card">
                    <span id="title_method">Метод ранга</span>
                    {problem.rang_result.map(res => {
                        return (
                            <div className="col-md-12 card_item">
                                <span  id="title">{res.name}</span>
                                <span  id="value">{res.value}</span>
                            </div>
                        )
                    })}

                </div>
                <div className="col-md-4 card ">
                    <span id="title_method">Метод полного парного сравнения</span>
                    {problem.rang_result.map(res => {
                        return (
                            <div className="col-md-12 card_item">
                                <span  id="title">{res.name}</span>
                                <span id="value">{res.value}</span>
                            </div>
                        )
                    })}

                </div>
            </div>
        </div>
    )

}
