import React, { useCallback, useEffect, useState, useContext } from 'react'
import { Link } from 'react-router-dom'

import { useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import { AuthContext } from '../context/AuthContex'

export const ListDecProblem = ({ result, title }) => {
    //console.log("RES_CHILD", result)
    return (
        <div className="container">
            <div className="row">
                <h1>{title}</h1>
            </div>
            <div className="col-md-12">
                <h2>Результаты</h2>
            </div>
            <p>{result.title}</p>
            <div>
                {result.ej_result.map(res => {
                    return(
                        <p>{res.name}</p>
                    )
                })}

            </div>
        </div>
    )

}