import React,{ useCallback,useEffect, useState, useContext} from 'react'
import {NavLink,Link,useParams} from 'react-router-dom'
import {useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'
import {ExpertList} from '../Components/ExpertsList'
import{Loader} from '../Components/Loader'


export const ExpertListPage = () =>{
    const {token} = useContext(AuthContext)
    const {request, loading} = useHttp()

    const [experts,setExperts] = useState([])
    const title = useParams().title

    const getExp = useCallback(async () =>{
        try {
            console.log(title)
            const fetched = await request('/api/auth/getexperts','POST',{title})
            console.log("FETCH",fetched)
            setExperts(fetched)
        } catch (e) {
            
        }
    },[token,title,request])


    useEffect(()=>{
        getExp()
    },[getExp])

    if(loading){
        return <Loader/>
    }
    return(
        <>
            {!loading && title && <ExpertList experts={experts} title={title}/>}
        </>
    )
}
