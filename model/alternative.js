const{Schema,model,Types} = require("mongoose")

const schema = new Schema({
    titleProblem:{type:String,require:true, unique:false},
    text:{type:String,require:true,unique:false},
})

module.exports = model('Alternative', schema)