import React, {useCallback, useEffect, useState, useContext} from 'react'
import {Link} from 'react-router-dom'
import {useHttp, } from '../../../hooks/http.hook'
import { useMessage } from '../../../hooks/message.hook'
import {AuthContext} from '../../../context/AuthContex'
import '../../../index.css'
export const EjCardExp = ({problem,title,alt}) =>{
    const [form,setForm]=useState({
        
      })
    const {loading,error,request,clearError}=useHttp()
    const message = useMessage()
    const {userName} = useContext(AuthContext)
    useEffect(()=>{
        message(error)
        clearError()  
      },[error,message,clearError])

    const changeHandler = event =>{
        setForm({ ...form, [event.target.name]:event.target.value })
    }

    const registerHandler = async () =>{
        form.title = problem.title
        form.userName = userName
        console.log({...form})
        try {
            const data = await request('/api/result/ej',"POST",{...form})
            message(data.message)
        } catch (e) {}
      }  
    return(
        <div className="container">
          <div className="row">
            <h1>{problem.title}</h1>
            <p>Поставьте каждой альтернативе оценку от 0 до 1. Чем выше значимость альтернативы, тем большую 
            оценку нужно ей дать. Сумма всех оценок должна ровняться 1.</p>
          </div>
          <div className="row">
            {alt.map(alt=>{
                return(
                    <div className="col-md-12 problem_container" >
                        <form>
                            <div className="header_problem">
                                <span className="ej_span">{alt.text}</span>
                                <span><input className="ej_input" type="text" size="3" id={alt.text} name={alt.text} placeholder="Оценка" onChange={changeHandler}/></span>
                            </div>
                            
                        </form>
                        </div>
                    )
            })}
            <div className="form-group">
                <button type="submit" className="btn btn-default" disabled={loading} onClick={registerHandler}>Отправить</button>
             </div>
          </div>
        </div>
    )
}