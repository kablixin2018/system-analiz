import React, { useEffect, useState, useContext} from 'react'
import {useHttp, } from '../../hooks/http.hook'
import { useMessage } from '../../hooks/message.hook'
import {AuthContext} from '../../context/AuthContex'

export const AddInformPage = ()=>{
    const auth = useContext(AuthContext)
    const message = useMessage()
    const {loading,error,request,clearError}=useHttp()
    const [form,setForm]=useState({
        username:auth.userName, nameWork:'', position: '', age:''
    })
    useEffect(()=>{
      message(error)
      clearError()  
    },[error,message,clearError])
  
    const changeHandler = event =>{
      setForm({ ...form, [event.target.name]:event.target.value })
    }
  
    const addHandler = async () =>{
      console.log({...form})
      try {
        const data = await request('/api/auth/update','POST',{...form})
        message(data.message)
      } catch (e) {}
    }

  
    return(
        <div className="container">
      <div className="row">
          <div className="col-md-offset-3 col-md-6 ">
              <form className="form-horizontal">
                  <span className="heading">Информация о {auth.userName}</span>
                  <div className="form-group">
                  <input
                       type="nameWork"
                       className="form-control" 
                       id="nameWork" 
                       name="nameWork"
                       placeholder="Место работы"
                       onChange={changeHandler}
                       />
                      <i className="fa fa-user"></i> 
                  </div>

                  <div className="form-group">
                  <input
                       type="position"
                       className="form-control" 
                       id="position" 
                       name="position"
                       placeholder="Должность"
                       onChange={changeHandler}
                       />
                      <i className="fa fa-user"></i> 
                  </div>
                  <div className="form-group">
                  <input
                       type="age"
                       className="form-control" 
                       id="age" 
                       name="age"
                       placeholder="Ваш возраст"
                       onChange={changeHandler}
                       />
                      <i className="fa fa-user"></i> 
                  </div>
                  <div className="form-group">
                      <button type="submit" className="btn btn-default" disabled={loading} onClick={addHandler}>Сохранить</button>
                  </div>
              </form>
          </div>
       </div>
      </div>
    )
}   