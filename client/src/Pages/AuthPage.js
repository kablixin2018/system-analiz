import React, { useEffect, useState, useContext} from 'react'
import {NavLink } from 'react-router-dom'
import '../style/AuthPage.css'
import {useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'


export const AuthPage = () =>{
  const auth = useContext(AuthContext)
  const message = useMessage()
  const {loading,error,request,clearError}=useHttp()
  const [form,setForm]=useState({
    email:'',password:'',userName:'',role:''
  })

  useEffect(()=>{
    message(error)
    clearError()  
  },[error,message,clearError])

  const changeHandler = event =>{
    setForm({ ...form, [event.target.name]:event.target.value })
  }

  const registerHandler = async () =>{
    console.log({...form})
    try {
      const data = await request('/api/auth/register','POST',{...form})
      message(data.message)
    } catch (e) {}
  }

  const loginHandler = async () =>{
    try {
      console.log('Form', {...form})
      const data = await request('/api/auth/login','POST',{...form})
      auth.login(data.token,data.userId,data.userName,data.role)
    } catch (e) {}
  }



    return (
     <div className="dada">
        <div className="container LogIn">
      <div className="row">
          <div className="col-md-offset-3 col-md-6 ">
              <form className="form-horizontal">
                  <span className="heading">АВТОРИЗАЦИЯ</span>
                  <div className="form-group">
                  <input
                       type="email"
                       className="form-control" 
                       id="email" 
                       name="email"
                       placeholder="E-mail"
                       onChange={changeHandler}
                       />
                      <i className="fa fa-user"></i> 
                  </div>
                  <div className="form-group help">
                      <input type="password" className="form-control" id="passwordLog" name="password" onChange={changeHandler} 
                      placeholder="Пароль"/>
                      <i className="fa fa-lock"></i>
                  </div>
                  <div className="form-group">
                      <button type="submit" className="btn btn-default" disabled={loading} onClick={loginHandler}>Вход</button>
                  </div>
              </form>
              <NavLink className="menu_item" to="/registration">Нет аккаунта?Зарегистрируйтесь!</NavLink>
          </div>
       </div>
      </div>
     </div>
    )
}