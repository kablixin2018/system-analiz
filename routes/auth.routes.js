const {Router}= require('express')
const User = require('../model/user')
const bcrypt = require('bcryptjs')
const config = require('config')
const jwt = require('jsonwebtoken')
const {check, validationResult} =require('express-validator')
const router = Router()

// /api/auth/register
router.post(
    '/register',
    [
        check('email','Неверный email').isEmail(),
        check('password','Минимальная длина пароля 6 символов').isLength({min:6}),
        check('username','Неверное имя! Минимальная длина 2 символа').isLength({min:2}),
        check('role','Неверная роль!').isLength({min:2})
    ],
    async (req,res)=>{
    try {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message:'Неккоректные данные при регистрации!'
            })
        }

        //console.log(req.body)
        console.log(req.body)
        const {email,password,username,role} = req.body

        const candidate = await User.findOne({email})
        
        if(candidate){
            return res.status(400).json({message:'Такой пользователь уже существует!'})
        }

        const hashedPassword = await bcrypt.hash(password, 12)
        const user = new User({email,password:hashedPassword,role,username})
        console.log(user)
        
        await user.save()
        res.status(201).json({message: 'Пользователь создан!'})

    } catch (e) {
        res.status(500).json({message:'Что-то пошло ни так! Попробуйте снова!'})
    }
})

// /api/auth/login
router.post(
    '/login',
    [
        check('password','Минимальная длина пароля 6 символов').exists(),
    ],
    async (req,res)=>{
    try {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message:'Неккоректные данные при входе!'
            })
        }

        const {email,password} = req.body
        console.log(req.body)

        const user = await User.findOne({email})
        console.log("USer", user)
        if(!user){
            return res.status(400).json({message: "Пользователь не найден!"})
        }

        const isMAth = await bcrypt.compare(password,user.password)

        if(!isMAth){
            return res.status(400).json({message: "Неверный пароль, попробуйте снова!"})
        }

        token = jwt.sign(
            {   userId: user._id, 
                email: user.email,
                username:user.username,
                role:user.role
            },
            config.get('jwtSecret'),
            {
                expiresIn:'1h'
            }
        )
        console.log('Token',token)
        res.json({token,userId:user.id,userName:user.username,role:user.role})
    } catch (e) {
        res.status(500).json({message:'Что-то пошло ни так! Попробуйте снова!'})
    }
})

router.post('/update',[],async(req,res)=>{
    try {
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message:'Неккоректные данные при входе!'
            })
        }
        console.log(req.body)
        User.findOneAndUpdate({username:req.body.username},{nameWork:req.body.nameWork,position:req.body.position,age:req.body.age},function(err,user){
            //Mongoose.disconnect();
            if(err) return console.log(err)
        })
        res.status(201).json({message:"Успешно!"})
    } catch (e) {
        
    }
})

router.post('/getexperts',async(req,res)=>{
    const result = await User.find({role:'Эксперт'})
    console.log(result)
    res.json(result)
})

module.exports = router
