import React, { useContext } from 'react'
import {NavLink, useHistory} from 'react-router-dom'
import {AuthContext} from '../context/AuthContex'


export const HeaderE = () =>{
    const History = useHistory()
    const auth = useContext(AuthContext)


    const logoutHandler = event =>{
        event.preventDefault()
        auth.logout()
        History.push('/')
    }
    return(
       <div className="header">
            <div className="container ">
                <div className="row">
                    <div className="col-md-3 main_link">
                        <span><button className="btn primary-btn"><a href="/">Главная</a></button></span>
                    </div>
                    <div className="col-md-9 menu">
                        <button className="btn primary-btn"><NavLink className="menu_item" to="/problems">Мои проблемы</NavLink></button>
                        <button className="btn primary-btn"><NavLink className="menu_item" to="/addInfo">Добавить информацию о себе</NavLink></button>
                        <button className ="btn"><a className="menu_item" href="/" onClick={logoutHandler}>Выйти</a></button>
                    </div>
                </div>
            </div>
       </div>
    )
}