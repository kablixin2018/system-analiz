import React, {useCallback, useContext, useEffect, useState} from 'react'
import {useParams} from 'react-router-dom'
import {useHttp} from '../../hooks/http.hook'
import {AuthContext} from '../../context/AuthContex'
import{ProblemCardExp} from '../../Components/ProblemCardExp'
import { Loader} from '../../Components/Loader'

export const ProblemViewPage = () =>{

    const {token} = useContext(AuthContext)
    const {request, loading} = useHttp()
    const [problem, setProblem] = useState([])
    const [alt,setAlt] = useState([])
    const [experts,setExperts] = useState([])
    const title = useParams().title

    const getProblem = useCallback(async () =>{
        try {
            console.log(title)
            const fetched = await request('/api/problem/getproblem','PUT',{title})
            setProblem(fetched)
        } catch (e) {
            
        }
    },[token,title,request])

    const fetchproblem = useCallback(async ()=>{
        try {
           const fetch = await request('/api/alt/getalt','POST',{title})
           setAlt(fetch)
        } catch (e) {}
    },[token,request])
    

    useEffect(()=>{
        getProblem()
    },[getProblem])

    useEffect(()=>{
        fetchproblem()
    },[fetchproblem])


    if(loading){
        return <Loader/>
    }
    return(
        <>
            {!loading && title && <ProblemCardExp alt={alt} problem={problem} title={title}/>}
        </>
    )
}