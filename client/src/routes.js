import React from 'react'
import {Switch, Route, Redirect} from 'react-router-dom'
import {AuthPage} from './Pages/AuthPage'
import{RegisterPage} from './Pages/RegisterPage'
import {MainPage} from './Pages/MainPage'
import {AddPage} from './Pages/AddPage'
import {ProblemViewPage}  from './Pages/ProblemViewPage'
import{ExpertListPage} from './Pages/ExpertListPage'
import{MainSendPage} from './Pages/MainSendPage'
import{MainDecPage} from './Pages/MainDecPage'
import{DecProblemViewPage} from './Pages/DecProblemViewPage'
export const useRoutes = (isAuthenticated,role) => {
    if(isAuthenticated){
        return(
            <Switch>
                <Route path="/index" >
                    <MainPage/>
                </Route>
                <Route path="/sendProblems" >
                    <MainSendPage/>
                </Route>
                <Route path="/decProblems" >
                    <MainDecPage/>
                </Route>
                <Route path="/problemView/:title">
                    <ProblemViewPage/>
                </Route>
                <Route path="/decproblemView/:title">
                    <DecProblemViewPage/>
                </Route>
                <Route path="/addExperts/:title">
                    <ExpertListPage/>
                </Route>
                <Route path="/addProblem/">
                    <AddPage/>
                </Route>
                <Redirect to="index" />
            </Switch>
        )
       
    }
    return(
        <Switch>
            <Route path="/" exact>
                <AuthPage/>
            </Route>
            <Route path="/registration" exact>
                <RegisterPage/>
            </Route>
            <Redirect to="/" />
        </Switch>

    )
}