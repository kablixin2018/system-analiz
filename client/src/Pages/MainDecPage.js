import React, { useCallback,useEffect, useState, useContext } from 'react'
import '../index.css'
import {useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'
import{ProblemList} from '../Components/ProblemList'
import {Loader} from '../Components/Loader'


export const MainDecPage = () =>{
    const {loading,request}=useHttp()

    const [problem,setproblem] = useState([])

    const {token,userName} = useContext(AuthContext)


    const fetchproblem = useCallback(async ()=>{
        try {
           const fetch = await request('/api/problem/get_dec_problems','PUT',{userName})
           setproblem(fetch)
           console.log("MAINPORB",{problem})
        } catch (e) {
            
        }
    },[token,request])
    useEffect(()=>{
        fetchproblem()
    },[fetchproblem])

    if(loading){
        return <Loader/>
    }
    return(
        <>
            {!loading &&<ProblemList problem={problem} user= {userName}/>}
        </>
    )
}