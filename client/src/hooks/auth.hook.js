import {useState,useCallback,useEffect} from 'react'

const storageName = 'userData'

export const useAuth = () => {
    const [token,setToken] = useState(null)
    const [userId,setUserId] = useState(null)
    const [userName,setUserName] = useState(null)
    const [role,setUserRole] = useState(null)

    const login = useCallback((jwtToken,userIdInput,userNameInput,userRole)=> {
        setToken(jwtToken)
        setUserId(userIdInput)
        setUserName(userNameInput)
        setUserRole(userRole)

        localStorage.setItem(storageName,JSON.stringify({
            token:jwtToken,userId:userIdInput,userName:userNameInput,role:userRole
        }))
    },[])

    const logout = useCallback(()=> {
        setToken(null)
        setUserId(null)
        setUserName(null)
        setUserRole(null)


        localStorage.removeItem(storageName)
    },[])

    useEffect(()=>{
        const data = JSON.parse(localStorage.getItem(storageName))
        if(data && data.token){
            login(data.token,data.userId,data.userName,data.role)
        }
    },[login])

    return {login,logout,token,userId,userName,role}
}
