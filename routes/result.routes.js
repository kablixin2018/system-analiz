const { Router } = require('express')
const Alternative = require('../model/alternative')
const Result_table = require('../model/result')
const { check, validationResult, Result } = require('express-validator')
const router = Router()
const Experts = require('../model/experts')
const Dec = require('../model/dec')
const Problems = require('../model/Problems')


router.post('/ej', async (req, res) => {
    const title = req.body.title
    const userName = req.body.userName
    delete req.body.userName
    delete req.body.title
    console.log("INPUT", req.body)
    const numbers = Object.values(req.body)
    console.log("NUMBERS", numbers)
    let sum = 0
    for (let i = 0; i < numbers.length; i++) {
        sum += Number(numbers[i])
    }
    console.log(sum)
    if (sum > 1 || sum < 1) {
        res.status(400).json({ message: "Сумма оценок должна быть равной единице!" })
    }
    const result = await Experts.find({ title: title })
    let weight = 0
    let sum_weight = 0
    for (let i = 0; i < result.length; i++) {
        if (result[i].username == userName) {
            weight = result[i].weight
        }
        sum_weight += result[i].weight
    }

    let weight_let = []
    const s = weight / sum_weight
    for (let i = 0; i < numbers.length; i++) {
        weight_let[i] = Number(numbers[i])
    }


    let result_sorted = []
    const alts = Object.keys(req.body)
    for (let i = 0; i < weight_let.length; i++) {
        let result_item = {}
        result_item.name = alts[i]
        result_item.value = weight_let[i]
        result_sorted.push(result_item)
    }
    let tmp = {}
    console.log(result_sorted)
    const answer = new Result_table({ title: title, method: 'ej', username: userName, result: result_sorted })
    await answer.save()
    res.status(201).json({ message: "Успешно" })
})

router.post('/pc', async (req, res) => {
    console.log(req.body)
    const userName = req.body.userName
    const title_problem = req.body.alts[0].titleProblem
    const alt = []
    let result = []
    let answer = req.body.answer
    for (let i = 0; i < req.body.alts.length; i++) {
        alt[i] = req.body.alts[i].text
    }
    for (let i = 0; i < alt.length; i++) {
        let result_item = {}
        result_item.name = alt[i]
        result_item.value = 0
        result.push(result_item)
    }

    for (let i = 0; i < answer.length; i++) {
        const choice = answer[i].value
        if (choice != 'eq') {
            for (let k = 0; k < result.length; k++) {
                if (result[k].name == choice)
                    result[k].value += 1
            }
        }
        else {
            for (let k = 0; k < result.length; k++) {
                if (result[k].name == answer[i].first_name || result[k].name == answer[i].second_name)
                    result[k].value += 0.5
            }
        }
    }

    const answers = new Result_table({ title: title_problem, method: 'pc', username: userName, result: result })
    await answers.save()

    res.status(201).json({ message: "Успешно" })
})

router.post('/pref', async (req, res) => {
    //console.log(req.body)
    const userName = req.body.userName
    const title = req.body.title
    const alts = req.body.alt
    const values = Object.values(alts)
    var isDuplicate = values.some(function (item, idx) {
        return values.indexOf(item) != idx
    });
    let validate = true
    //Проверка
    for (let i = 0; i < values.length; i++) {
        if (Number(values[i]) <= 0) {
            validate = false
        }
    }
    if (isDuplicate && validate) {
        return res.status(401).json({ message: "Вы не правильно ввели значения! Значения не должны повторяться!" })
    }
    if (!validate) {
        return res.status(402).json({ message: "Вы не ввели значения! Необходимо заполнить все поля!" })
    }
    let result = []
    const keys = Object.keys(alts)
    for (let i = 0; i < values.length; i++) {
        let result_item = {}
        result_item.name = keys[i]
        result_item.value = values[i]
        result.push(result_item)
    }

    const rang = new Result_table({ title: title, method: "pref", username: userName, result: result })
    await rang.save()
    res.status(201).json({ message: "Успешно!" })
})

router.post('/rang', async (req, res) => {
    //Добавить проверки!!!!
    console.log(req.body)
    const userName = req.body.userName
    const title = req.body.title
    const alts = req.body.alt
    const values = Object.values(alts)
    var isDuplicate = values.some(function (item, idx) {
        return values.indexOf(item) != idx
    });
    let validate = true
    //Проверка
    for (let i = 0; i < values.length; i++) {
        if (Number(values[i]) <= 0 && Number(values[i] > 10)) {
            validate = false
        }
    }
    if (!validate) {
        return res.status(401).json({ message: "Заполните все поля!" })
    }
    let result = []
    const keys = Object.keys(alts)
    for (let i = 0; i < values.length; i++) {
        let result_item = {}
        result_item.name = keys[i]
        result_item.value = values[i]
        result.push(result_item)
    }

    const rang = new Result_table({ title: title, method: "rang", username: userName, result: result })
    await rang.save()
    res.status(201).json({ message: "Успешно!" })
})

router.post('/fpm', async (req, res) => {
    console.log(req.body)
    const answer = req.body.answer
    const username = req.body.userName
    const title = req.body.alts[0].titleProblem
    console.log("ALT", answer)
    console.log("username", username)
    console.log("tite", title)
    const alt = await Alternative.find({ titleProblem: title })
    let result = []
    const problemR = await Problems.find({title:title})
    const n= problemR[0].value
    console.log(n)
    for (let i = 0; i < alt.length; i++) {
        let result_item = {}
        result_item.name = alt[i].text
        result_item.value = 0
        result.push(result_item)
    }
    for (let i = 0; i < answer.length; i++) {
        for (let j = 0; j < result.length; j++) {
            if (result[j].name == answer[i].value) {
                result[j].value += Number(answer[i].size)
            }
        }
        if (answer[i].first_name == answer[i].value) {
            for (let j = 0; j < result.length; j++) {
                if (result[j].name == answer[i].second_name) {
                    result[j].value += n - Number(answer[i].size)
                }
            }
        }
        if (answer[i].second_name == answer[i].value) {
            for (let j = 0; j < result.length; j++) {
                if (result[j].name == answer[i].first_name) {
                    result[j].value += n - Number(answer[i].size)
                }
            }
        }
    }
    console.log(result)
    const fpm = new Result_table({ title: title, method: "fpm", username: username, result: result })
    await fpm.save()
    res.status(201).json({ message: "Успешно!" })
})

router.post('/check_dec', async (req, res) => {
    try {
        if (!req.body) return res.status(401).json({ message: "Ошибка при отправлении данных" })
        console.log(req.body)
        const methods = await Result_table.find({ username: req.body.userName, title: req.body.title })
        let result = { pc: false, fpm: false, rang: false, ej: false, pref: false }
        for (let i = 0; i < methods.length; i++) {
            if (methods[i].method == "ej") {
                result.ej = true
            }
            if (methods[i].method == "pc") {
                result.pc = true

            }
            if (methods[i].method == "pref") {
                result.pref = true

            }
            if (methods[i].method == "rang") {
                result.rang = true
            }
            if (methods[i].method == "fpm") {
                result.fpm = true
            }
        }
        console.log(result)
        res.json(result)
    } catch (e) {
        return res.status(501).json({ message: "Что-то пошло ни так!" })
    }
})

async function fpm(username, title) {
    const experts = await Experts.find({ title: title })
    const problem_rang = await Problems.find({title:title})
    //Находим S 
    let S = []
    for (let i = 0; i < experts.length; i++) {
        let S_item = {}
        let sum = 0
        S_item.name = experts[i].username
        let Z = await Result_table.find({ username: experts[i].username, method: "fpm", title: title })
        let string = JSON.parse(JSON.stringify(Z[0].result))
        for (let j = 0; j < string.length; j++) {
            sum += Number(string[j].value)
        }
        S_item.value = sum;
        S.push(S_item)
    }
    const tmp_alt = await Alternative.find({ titleProblem: title })
    const n = tmp_alt.length
    const N = n * (n - 1)
    //Найдем все альтернативы 
    let alt = []
    for (let i = 0; i < tmp_alt.length; i++) {
        let tmp_item = {}
        tmp_item.name = tmp_alt[i].text
        tmp_item.value = 0
        alt.push(tmp_item)
    }

    //Вычислим веса
    for (let i = 0; i < S.length; i++) {
        Z = await Result_table.find({ title: title, username: S[i].name, method: 'fpm' })
        let string = JSON.parse(JSON.stringify(Z[0].result))

        for (let j = 0; j < string.length; j++) {
            for (k = 0; k < alt.length; k++) {
                if (string[j].name == alt[k].name) {
                    alt[k].value += Number(string[j].value)
                }
            }
        }
    }
    //Y
    for (let i = 0; i < alt.length; i++) {
        alt[i].value/=N
    }
    
    for (let i = 0; i < alt.length - 1; i++) {
        let tmp = {}
        let Number1 = Number(alt[i].value)
        let Number2 = Number(alt[i + 1].value)
        if (Number2 > Number1) {
            tmp = alt[i]
            alt[i] = alt[i + 1]
            alt[i + 1] = tmp
        }
    }
    return alt
}

async function rang(username, title) {
    const experts = await Experts.find({ title: title })
    //Находим S 
    let S = []
    for (let i = 0; i < experts.length; i++) {
        let S_item = {}
        let sum = 0
        S_item.name = experts[i].username
        let Z = await Result_table.find({ username: experts[i].username, method: "rang", title: title })
        let string = JSON.parse(JSON.stringify(Z[0].result))
        for (let j = 0; j < string.length; j++) {
            sum += Number(string[j].value)
        }
        S_item.value = sum;
        S.push(S_item)
    }
    const m = S.length
    const tmp_alt = await Alternative.find({ titleProblem: title })
    //Составляем P
    let P = []
    for (let i = 0; i < S.length; i++) {
        let Z1 = await Result_table.find({ title: title, username: S[i].name, method: 'pref' })
        let string = JSON.parse(JSON.stringify(Z1[0].result))
        P.push(string)

    }
    console.log("P", P)
    //Составляем R
    //let R = []
    for (let i = 0; i < P.length; i++) {
        for (let j = 0; j < P[i].length; j++) {
            P[i][j].value = Number(P[i][j].value) / Number(S[i].value)
        }
    }


    //Находим V
    let alt = []
    for (let i = 0; i < tmp_alt.length; i++) {
        let alt_item = {}
        let sum = 0
        alt_item.name = tmp_alt[i].text
        for (let j = 0; j < P.length; j++) {
            for (let k = 0; k < P[j].length; k++) {
                if (P[j][k].name == tmp_alt[i].text) {
                    sum += Number(P[j][k].value) / m
                }
            }
        }
        alt_item.value = sum
        alt.push(alt_item)
    }

    console.log("ALT asdsa", alt)
    for (let i = 0; i < alt.length - 1; i++) {
        let tmp = {}
        let Number1 = Number(alt[i].value)
        let Number2 = Number(alt[i + 1].value)
        if (Number2 > Number1) {
            tmp = alt[i]
            alt[i] = alt[i + 1]
            alt[i + 1] = tmp
        }
    }

    return alt


}

async function pref(username, title) {
    const experts = await Experts.find({ title: title })
    console.log("TITLE", title)
    //Находим S 
    let S = []
    for (let i = 0; i < experts.length; i++) {
        let S_item = {}
        let sum = 0
        S_item.name = experts[i].username
        let Z = await Result_table.find({ username: experts[i].username, method: "rang", title: title })
        let string = JSON.parse(JSON.stringify(Z[0].result))
        for (let j = 0; j < string.length; j++) {
            sum += Number(string[j].value)
        }
        S_item.value = sum;
        S.push(S_item)
    }
    const m = S.length
    const tmp_alt = await Alternative.find({ titleProblem: title })
    //Составляем P
    let P = []
    for (let i = 0; i < S.length; i++) {
        let Z1 = await Result_table.find({ title: title, username: S[i].name, method: 'pref' })
        let string = JSON.parse(JSON.stringify(Z1[0].result))
        P.push(string)

    }
    const n = tmp_alt.length
    //Составляем K
    for (let i = 0; i < P.length; i++) {
        for (let j = 0; j < P[i].length; j++) {
            P[i][j].value = n - Number(P[i][j].value)
        }
    }
    //Находим Li
    let alt = []
    for (let i = 0; i < tmp_alt.length; i++) {
        let alt_item = {}
        let sum = 0
        alt_item.name = tmp_alt[i].text
        for (let j = 0; j < P.length; j++) {
            for (let k = 0; k < P[j].length; k++) {
                if (P[j][k].name == tmp_alt[i].text) {
                    sum += Number(P[j][k].value)
                }
            }
        }
        alt_item.value = sum
        alt.push(alt_item)
    }
    //Находим L
    let sum_all = 0
    for (let i = 0; i < alt.length; i++) {
        sum_all += Number(alt[i].value)
    }
    //Резудьтат
    for (let i = 0; i < alt.length; i++) {
        alt[i].value = alt[i].value / sum_all
    }
    console.log("ALT asdsa", alt)
    for (let i = 0; i < alt.length - 1; i++) {
        let tmp = {}
        let Number1 = Number(alt[i].value)
        let Number2 = Number(alt[i + 1].value)
        if (Number2 > Number1) {
            tmp = alt[i]
            alt[i] = alt[i + 1]
            alt[i + 1] = tmp
        }
    }
    return alt
    //console.log("N", n)
    //console.log("RESUlt", P)
    console.log("ALT", alt)
    //console.log("S",S)
}

async function ej(username, title) {
    let result = []
    const experts = await Experts.find({ title: title })
    const tmp_alt = await Alternative.find({ titleProblem: title })
    //Общий вес
    let sum_weight = 0
    for (let i = 0; i < experts.length; i++) {
        sum_weight += experts[i].weight
    }

    //Вычисляем веса
    let S = []
    for (let i = 0; i < experts.length; i++) {
        let S_item = {}
        S_item.name = experts[i].username
        S_item.weight = Number(experts[i].weight) / sum_weight
        S.push(S_item)
    }
    //Найдем все альтернативы 
    
    console.log("ALT", tmp_alt)
    let alt = []
    for (let i = 0; i < tmp_alt.length; i++) {
        let tmp_item = {}
        tmp_item.name = tmp_alt[i].text
        tmp_item.value = 0
        alt.push(tmp_item)
    }

    //Вычислим веса
    for (let i = 0; i < S.length; i++) {
        Z = await Result_table.find({ title: title, username: S[i].name, method: 'ej' })
        let string = JSON.parse(JSON.stringify(Z[0].result))

        for (let j = 0; j < string.length; j++) {
            for (k = 0; k < alt.length; k++) {
                if (string[j].name == alt[k].name) {
                    alt[k].value += Number(S[i].weight) * Number(string[j].value)
                }
            }
        }
    }
    console.log("ALT", alt)
    for (let i = 0; i < alt.length - 1; i++) {
        let tmp = {}
        let Number1 = Number(alt[i].value)
        let Number2 = Number(alt[i + 1].value)
        if (Number2 > Number1) {
            tmp = alt[i]
            alt[i] = alt[i + 1]
            alt[i + 1] = tmp
        }
    }
    console.log("ALT", alt)
    return alt
}

async function pc(username,title) {
    const data_exp = await Result_table.find({ username: username,title:title, method: "pc" })
    console.log("Dadadada",data_exp)
    let string = JSON.parse(JSON.stringify(data_exp[0])).result
    let result = []
    let all_sum = 0;
    for (let i = 0; i < string.length; i++) {
        all_sum += string[i].value
    }
    for (let i = 0; i < string.length; i++) {
        let result_item = {}
        result_item.name = string[i].name
        result_item.value = string[i].value / all_sum
        result.push(result_item)
    }
    console.log("RESULT NO SORT", result)
    for (let i = 0,endI=result.length-1; i <endI - 1; i++) {
        for(let j=0,endJ=endI-i;j<endJ;j++){
            let tmp = {}
            let Number1 = Number(result[j].value)
            let Number2 = Number(result[j + 1].value)
            if (Number2 < Number1) {
                tmp = result[j]
                result[j] = result[j + 1]
                result[j + 1] = tmp
            }
        }
    }
    //console.log("DATA_EXP",string)
    console.log("Result", result)
    return result
}


router.post('/get_result', async (req, res) => {
    if (!req.body) return res.status(401).json({ message: "Неверно переданы данные!" })
    try {
        const experts = await Experts.find({ title: req.body.title, status: 'dec' })
        let pc_massive =[]
        for(let i = 0; i<experts.length;i++){
            
            const pc_result = await pc(experts[i].username,req.body.title)
            console.log("PC_RESULT", pc_result)
            pc_massive.push(pc_result)
        }
        const ej_result = await ej(experts[0].username,req.body.title )
        const pref_result = await pref(experts[0].username,req.body.title )
        const rang_result = await rang(experts[0].username,req.body.title)
        const fpm_result = await fpm(experts[0].username, req.body.title)
        console.log("SADASDAD")
        const newObj = new Dec({title:req.body.title,ej_result:ej_result,pref_result:pref_result,rang_result:rang_result,
                                fpm_result:fpm_result,pc_result:pc_massive})
        console.log("New obj",newObj)               
        await newObj.save()
        
        res.json({ej_result:ej_result, pref_result:pref_result, rang_result:rang_result,fpm_result:fpm_result,pc_result:pc_massive})
    } catch (e) {
        return res.status(501).json({message:"Что-то пошло ни так!"})
    }
})

router.put('/get_result_from_bd',async(req,res)=>{
    try {
        const data = await Dec.find({title:req.body.title})
        console.log(data)
        return res.json(data)
    } catch (e) {
        return res.status(501).json({message:"Что-пошло ни так!"})
    }
})
module.exports = router

