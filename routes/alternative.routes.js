const {Router}= require('express')
const Alternative = require('../model/alternative')
const config = require('config')
const {check, validationResult} =require('express-validator')
const router = Router()

router.post('/getalt',async(req,res)=>{
    const result = await Alternative.find({titleProblem:req.body.title})
    res.status(201).json(result)
})


router.post(
    '/addAlt',
    [
    ],
    async(req,res)=>{
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message:'Неккоректные данные при добавлении!'
            })
        }
        const candidate = await Alternative.findOne({text:req.body.text})
        
        if(candidate){
            return res.status(400).json({message:'Такая альтернатива уже существует!'})
        }
        console.log("its ALT",req.body)
        const result = new Alternative({titleProblem:req.body.titleProblem,text:req.body.text})
        await result.save()
        res.status(201).json({message:"Успешно!"})
})

router.post('/delete',async(req,res)=>{
    console.log(req.body)  
    await Alternative.deleteOne({_id:req.body._id})
    res.status(201).json({message:"Успешно удалено! Обновите страницу"}) 
})
module.exports = router