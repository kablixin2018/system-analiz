import React, { useContext } from 'react'
import {NavLink, useHistory} from 'react-router-dom'
import {AuthContext} from '../context/AuthContex'
import '../style/HeaderA.css'

export const HeaderA = () =>{
    const History = useHistory()
    const auth = useContext(AuthContext)


    const logoutHandler = event =>{
        event.preventDefault()
        auth.logout()
        History.push('/')
    }
    return(
       <div className="header">
            <div className="container ">
                <div className="row">
                    <div className="col-md-3 main_link">
                        <span><button className="btn primary-btn"><a href="/">Мои проблемы</a></button></span>
                    </div>
                    <div className="col-md-9 menu">
                        <button className="btn primary-btn"><NavLink className="menu_item" to="/sendProblems">Отправлено экспертам</NavLink></button>
                        <button className="btn primary-btn"><NavLink className="menu_item" to="/decProblems">Решенные проблемы</NavLink></button>
                        <button className="btn primary-btn"><NavLink className="menu_item" to="/addProblem">Добавить проблему</NavLink></button>
                        <button className ="btn"><a className="menu_item" href="/" onClick={logoutHandler}>Выйти</a></button>
                    </div>
                </div>
            </div>
       </div>
    )
}