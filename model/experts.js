const{Schema,model,Types} = require("mongoose")

const schema = new Schema({
    title:{type:String,require:true, unique:false},
    username:{type:String,require:true,unique:false},
    weight:{type:Number,require:true,unique:false},
    status:{type:String,require:false,unique:false,default:'nodec'}
})

module.exports = model('Experts', schema)