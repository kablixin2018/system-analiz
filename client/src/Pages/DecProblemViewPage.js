import React, { useCallback, useContext, useEffect, useState } from 'react'
import { useParams } from 'react-router-dom'
import { useHttp } from '../hooks/http.hook'
import { AuthContext } from '../context/AuthContex'
import { DecProblemCard } from '../Components/DecProblemCard'
import { Loader } from '../Components/Loader'

export const DecProblemViewPage = () => {

    const { token } = useContext(AuthContext)
    const { request, loading } = useHttp()
    const [problem, setproblem] = useState([])

    const title = useParams().title
    const [link, setLink] = useState(null)

    const getLink = useCallback(async () => {
        try {
            const fetched = await request('/api/result/get_result_from_bd', 'PUT', { title })
            setLink(fetched[0])
        } catch (e) { }
    }, [token, title, request])

    useEffect(() => {
        getLink()
    }, [getLink])

    if (loading) {
        return <Loader />
    }

    return (
        <>
            {!loading && title && { problem } && <DecProblemCard problem={link} title={title} />}
        </>
    )



}