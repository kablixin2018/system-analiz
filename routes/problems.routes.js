const {Router} = require('express')
const Problem = require('../model/Problems')
const Experts = require('../model/experts')
const {Mongoose} = require("mongoose")
const config = require('config')
const Alternative = require('../model/alternative')
const {check,validationResult} = require('express-validator')
const Problems = require('../model/Problems')
const fetch = require('node-fetch')
const router = Router()

router.post(
    '/add',
    [
        check('title','Неверный заголовок').isLength({min:5}),
        check('main','Неверная формулировка проблемы').isLength({min:10})
    ],
    async(req,res)=>{
        const errors = validationResult(req)
        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message:'Неккоректные данные при добавлении!'
            })
        }
        const {title,main,username,value} = req.body
        const candidate = await Problem.findOne({title})
        if(candidate){
            return res.status(401).json({message:'Проблема с таким заголовком уже сущетсвует'})
        }
        const problem = new Problem({title,main,username,value})
        await problem.save()
        res.status(200).json({message:"Проблема успешно добавлена!"})
    }
)
router.put('/problems',async(req,res) =>{
    try {
       
        const problems = await Problem.find({username:req.body.userName,status:'nosend'})
        res.json(problems)
    } catch (e) {
        res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
    }
})

router.put('/problems_expert',async(req,res) =>{
    try {
        console.log(req.body)
        result=[]
        const problems = await Experts.find({username:req.body.userName})
        console.log("PROBLEMS", problems[0])
        for(let i=0;i<problems.length;i++){
            let tmp = await Problem.find({title:problems[i].title})
            console.log(i)
            console.log("TMP",tmp)
            if(tmp[0].status=='send'){
                let field = {_id:'',status:'',title:'',main:'',username:''}
                field._id = tmp[0]._id
                field.status = tmp[0].status
                field.title = tmp[0].title
                field.main = tmp[0].main
                field.username=tmp[0].username
                console.log("FIEDL",field)
                result.push(field)
            }
        }
        console.log(result)
        res.json(result)
    } catch (e) {
        res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
    }
})

router.put('/get_problems_expert',async(req,res)=>{
    if(!req.body) return res.status(401).json({message:"Ошибка при отправки данных"})
    try {
        console.log()
        const problems = await Problem.find({status:'send'})
        const expert = await Experts.find({username:req.body.userName,status:'nodec'})
        let result = []
        console.log("PROBLEMS",problems,"EXPERT",expert)
        for(let i = 0; i<problems.length;i++){
            for(let j = 0; j<expert.length;j++){
                if(problems[i].title==expert[j].title){
                    result.push(problems[i])
                }
            }
        }
        res.json(result)
    } catch (e) {
        res.status(501).json({message:"Что-то пошло ни так!"})
    }
})

router.put('/send_problems',async(req,res) =>{
    try {
        console.log(req.body.userName)
        const problems = await Problem.find({username:req.body.userName,status:'send'})
        res.json(problems)
    } catch (e) {
        res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
    }
})
router.post('/send',async(req,res)=>{
    try {
        console.log(req.body)
        const alt = await Alternative.find({titleProblem:req.body.title})
        if(alt.length == 0){
            return res.status(401).json({message:"Ошибка! Проблема не имеет альтернтив!"})
        }
        const exp = await Experts.find({title:req.body.title}) 
        if(exp.length == 0){
            return res.status(402).json({message:"Ошибка! К проблеме не назначено экспертов!"})
        }
        console.log(alt,exp)
        await Problem.findOneAndUpdate({title:req.body.title},{status:'send'})
        res.status(201).json({message:"Успешно"})
    } catch (e) {
        res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
    }
})
router.put('/getproblem',async(req,res)=>{
    try {
        console.log(req.body)
        const jsons = await Problem.findOne({title:req.body.title})
        console.log(jsons)
        res.json(jsons)
    } catch (e) {
        
    }
})

router.post('/delete',async(req,res)=>{
    try {
        console.log(req.body)  
        await Problem.deleteOne({title:req.body.title})
        res.status(201).json({message:"Успешно удалено! Обновите страницу"}) 
    } catch (e) {
        
    }
})

router.post('/dec_send_problem',async(req,res)=>{
    if(req.body=={}) return res.status(401).json({message:"Ошибка при передачи данных!"})
    try {
        console.log("asdasdasd",req.body)
        //await Problem.findOneAndUpdate(req.body.title,{status:'dec'})
        await Experts.findOneAndUpdate({title:req.body.title,username:req.body.userName},{status:'dec'})
        console.log("asdasdasd",req.body)
        const check = await Experts.find({title:req.body.title,status:'nodec'})
        console.log(check)
        if(check.length == 0){
            console.log('asdasdasdasdasdasd')
            await Problem.findOneAndUpdate(req.body.title,{status:'dec'})
            const send_feth={
                title:req.body.title
            }
            const data = await fetch("http://localhost:5000/api/result/get_result",{
                method:'post',
                headers:{
                    "content-type":'application/json'
                },
                body: JSON.stringify(send_feth)
            })
            console.log("Получил инфу")
            const json = data.json()
            console.log(json)
        }
        res.status(201).json({message:"Успешно!"})
    } catch (e) {
        return res.status(501).json({message:"Что-то пошло ни так"})
    }
})

router.put('/get_dec_problems',async(req,res)=>{
    try {
        console.log(req.body.userName)
        const problems = await Problem.find({username:req.body.userName,status:'dec'})
        res.json(problems)
    } catch (e) {
        res.status(500).json({message:"Что-то пошло ни так, попробуйте снова!"})
    }
})
module.exports = router