import React, {useCallback, useEffect, useState, useContext} from 'react'
import {Link} from 'react-router-dom'
import {useHttp, } from '../../../hooks/http.hook'
import { useMessage } from '../../../hooks/message.hook'
import {AuthContext} from '../../../context/AuthContex'

export const PcCardExp = ({problem,title,alt}) =>{
    const auth = useContext(AuthContext)
    const message = useMessage()
    const {loading,error,request,clearError}=useHttp()
    const [form,setForm]=useState({
        name:'',value:''
      })
    let forms = []
    useEffect(()=>{
      message(error)
      clearError()  
    },[error,message,clearError])
  

    const changeHandler = (event,second) =>{
        console.log("event.target.name",event.target.name)
        console.log("event.target.value",event.target.value)
        let input_item = {
            first_name: event.target.name,
            second_name: second.text,
            value: event.target.value
        }
        for(let i = 0;i<forms.length;i++){
            if(input_item.first_name == forms[i].second_name && input_item.second_name == forms[i].first_name){
                forms[i]= input_item
            }
        }
        forms.push(input_item)
        console.log(forms)
    }
    const tmp_alt = alt


    const sendHandeler = async () =>{
        console.log(forms)
        console.log(auth.userName)
        const data_res = {
            userName: auth.userName,
            alts: alt,
            answer: forms
        }
        try {
            console.log("FORMS",forms)
            const data = await request('/api/result/pc',"POST", data_res)
            message(data.message)
        } catch (e) {}
      }
    return(
        <div className="container">
            <div className="row">
                <h1>{problem.title}</h1>
                <p>Сравните альтернативы. Для каждой пары выберите наиболее предпочтительную альтернативу, 
                    либо отметьте, что альтернативы однозначны.</p>
            </div>
            <div className="row">
                {alt.map(alt=>{
                    return(
                        <div className="col-md-12 problem_container" >
                            <div className="header_problem">
                                <span>{alt.text}</span>
                            </div>
                            </div>
                        )
                })}
            </div>

            <div className="row">
                <form name="pc">
                    <table>
                        {alt.map(alt_current=>{
                            return(
                                <div className="col-md">
                                    {tmp_alt.map(tmp_alt_current=>{
                                        let parent = alt.indexOf(alt_current)
                                        let children =tmp_alt.indexOf(tmp_alt_current) 
                                        if(tmp_alt_current.text!=alt_current.text && parent<children)
                                            return(
                                                <div>
                                                    <th nowrap>
                                                       <span>{alt_current.text}</span>
                                                    </th>
                                                    <th width="10px">

                                                    </th>
                                                    <th  nowrap>
                                                        <span>{tmp_alt_current.text}</span>

                                                    </th>
                                                    <th width="10">
                                                    </th>
                                                    <th align="right " nowrap width="100px">
                                                        <select name={alt_current.text} onChange={(event)=>changeHandler(event,tmp_alt_current)}>
                                                            <option selected value="Крокодил Гена" disabled>Выберите из списка</option>
                                                            <option value = {alt_current.text}>Альтернатива 1</option>
                                                            <option value = {tmp_alt_current.text}>Альтернатива 2</option>
                                                            <option value ="eq">Альтернативы равнозначны</option>
                                                        </select>
                                                    </th>
                                            </div>
                                            )
                                        console.log(tmp_alt.indexOf(alt_current))
                                        
                                    })}
                                </div>
                            )
                        })}
                </table>
                </form>
            </div>
            <div className="form-group">
                      <button type="submit" className="btn btn-default" disabled={loading} onClick={sendHandeler}>Отправить</button>
                </div>
        </div>
    )
}