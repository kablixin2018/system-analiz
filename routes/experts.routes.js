const {Router}= require('express')
const Experts = require('../model/experts')
const config = require('config')
const {check, validationResult} =require('express-validator')
const router = Router()

router.post('/getExperts',async(req,res)=>{
    console.log(req.body)
    const result = await Experts.find({title:req.body.title})
    console.log("Exp",result)
    res.status(201).json(result)
})


router.post(
    '/add',
    [
    ],
    async(req,res)=>{
        
        console.log("its EXP",req.body)
        const exp = new Experts({title:req.body.title,username:req.body.username,weight:req.body.weight})
        await exp.save()
        res.status(201).json({message:"Успешно!"})
})

router.post('/delete',async(req,res)=>{
    console.log(req.body)  
    await Experts.deleteOne({_id:req.body._id})
    res.status(201).json({message:"Успешно удалено! Обновите страницу"}) 
})

router.post('/check_dec', async(req,res)=>{
    
})
module.exports = router