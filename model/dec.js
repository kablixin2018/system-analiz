const{Schema,model,Types} = require("mongoose")

const schema = new Schema({
    title:{type:String, require:false, unique:false},
    ej_result:[{
        name:{type:String,require:true,unique:false},
        value:{type:Number,require:true,unique:false}
    }],
    pref_result:[{
        name:{type:String,require:true,unique:false},
        value:{type:Number,require:true,unique:false}
    }],
    rang_result:[{
        name:{type:String,require:true,unique:false},
        value:{type:Number,require:true,unique:false}
    }],
    fpm_result:[{
        name:{type:String,require:true,unique:false},
        value:{type:Number,require:true,unique:false}
    }],
    pc_result:[
            [{
                name:{type:String,require:true,unique:false},
                value:{type:Number,require:true,unique:false}
            }]  
    ]
})

module.exports = model('Dec', schema)