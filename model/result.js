const{Schema,model,Types} = require("mongoose")

const schema = new Schema({
    title:{type:String, require:false, unique:false},
    method:{type:String,require:true,unique:false},
    username:{type:String,require:true,unique:false},
    result:[{
        name:{type:String,require:true,unique:false},
        value:{type:Number,require:true,unique:false}
    }]
})

module.exports = model('Result', schema)