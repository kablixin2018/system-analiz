import React,{ useEffect, useState, useContext} from 'react'
import {useHttp, } from '../hooks/http.hook'
import { useMessage } from '../hooks/message.hook'
import {AuthContext} from '../context/AuthContex'

import '../index.css'

export const ExpertList = ({experts,title}) =>{
    const auth = useContext(AuthContext)
    
    const message = useMessage()
    
    const {loading,error,request,clearError}=useHttp()
    
    const [form,setForm]=useState({
      title:title,username:'',weight:''
    })
   
    useEffect(()=>{
        message(error)
        clearError()  
      },[error,message,clearError])

    if(!experts.length){
        return <p className="center">Экспертов в системе пока нет!</p>
    }
    const changeHandler = event =>{
        setForm({ ...form, [event.target.name]:event.target.value })
      }
    const addHandler=(username) =>{
        form.username = username
        addfunc()
    }

    const addfunc = async()=>{
        try {
            const data = await request('/api/experts/add','POST',{...form})
            message(data.message)
        } catch (e) {
            
        }
    }
    return(
        <div className="container all_problems_container">
            <div className="row">
                {experts.map(exp=>{
                        return(
                            <div className="col-md-12 problem_container" >
                                <div className="header_problem">
                                    <span className="user_info">{exp.username}</span>
                                    <span className="user_info">{exp.nameWork}</span>
                                    <span className="user_info">{exp.position}</span>
                                    <input
                                        type="text"
                                        className="form-control" 
                                        id="weight" 
                                        name="weight"
                                        placeholder="weight"
                                        onChange={changeHandler}
                                        />
                                </div>
                                <div>
                                    <button className="btn btn-danger" onClick={()=>addHandler(exp.username)}>Выбрать эксперта</button>
                                </div>
                            </div>
                        )
                    })}
            </div>
        </div>
    )
}