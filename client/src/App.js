import './App.css'
import React from 'react'
import {BrowserRouter as Router} from 'react-router-dom'
import { useRoutes } from './routes';
import { useRoutesExp } from './routesExp';
import {useAuth} from './hooks/auth.hook'
import {AuthContext} from './context/AuthContex'
import {HeaderA} from './Components/HeaderA'
import {HeaderE} from './Components/HeaderE'

function App() {
  const {login,logout,token,userId,userName,role}=useAuth()
  const isAuthenticated = !!token
  const routes = useRoutes(isAuthenticated,role)
  const routesExp = useRoutesExp(isAuthenticated,role)
  if(role == "Аналитик"){
    return (
      <AuthContext.Provider value={{
       login,logout,token,userId,userName,role,isAuthenticated 
      }}>
         <Router>      
              {isAuthenticated&&<HeaderA/>}       
           <div className="containers">
             {routes}
           </div>
       </Router>
      </AuthContext.Provider>
     );
  }
  else{
    return (
      <AuthContext.Provider value={{
       login,logout,token,userId,userName,role,isAuthenticated 
      }}>
         <Router>      
              {isAuthenticated&&<HeaderE/>}       
           <div className="containers">
             {routesExp}
           </div>
       </Router>
      </AuthContext.Provider>
     );
  }
}

export default App
