const{Schema,model,Types} = require("mongoose")

const schema = new Schema({
    title:{type:String,require:true, unique:true},
    main:{type:String,require:true,unique:false},
    username:{type:String,require:true,unique:false},
    value:{type:Number,require:true,unique:false},
    status:{type:String,require:true,unique:false,default:'nosend'}
})
 
module.exports = model('Problems', schema)