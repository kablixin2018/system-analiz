import React,{ useEffect, useState, useContext} from 'react'
import {NavLink,Link} from 'react-router-dom'
import {useHttp, } from '../../../hooks/http.hook'
import { useMessage } from '../../../hooks/message.hook'
import {AuthContext} from '../../../context/AuthContex'
import '../../../style/ProblemPage.css'
import {ProblemViewPage} from '../ProblemViewPage'



export const ProblemList = ({problem},{username}) =>{
    const auth = useContext(AuthContext)
    
    const message = useMessage()
    
    const {loading,error,request,clearError}=useHttp()
    
    const [form]=useState({
      userId:auth.userId,title:''
    })
   
    useEffect(()=>{
        message(error)
        clearError()  
      },[error,message,clearError])

    if(!problem.length){
        return <p className="center">Проблем в системе пока нет!</p>
    }

    const deleteFunction = (title) =>{
        form.title =title
        changedHandler()
    } 
    const sendFunction = (title) =>{
        form.title =title
        sendHandler()
    } 
    const changedHandler = async ()=>{
        try {
            const data = await request('/api/problem/delete','POST',{...form})
            message(data.message)
        } catch (e) {
            
        }
    }
    const sendHandler = async ()=>{
        try {
            const data = await request('/api/problem/send','POST',{...form})
            message(data.message)
        } catch (e) {
            
        }
    }
    
    return(
        <div className="container all_problems_container">
            <div className="row">
                {problem.map(problem=>{
                        if(problem.status =="send"){
                            return(
                                <div className="col-md-12 problem_container" >
                                    <div className="header_problem">
                                        <span>{problem.title}</span>
                                    </div>
                                    <div className="body_problem">
                                        <Link className="btn btn-primary" to={`problemView/${problem.title}`}> Открыть проблему
                                        </Link>
                                    </div>
                                </div>
                            )
                        }
                        else{
                            return(
                                <div className="col-md-12 problem_container" >
                                    <div className="header_problem">
                                        <span>{problem.title}</span>
                                    </div>
                                    <div className="body_problem">
                                        <Link className="btn btn-primary" to={`problemView/${problem.title}`}> Открыть проблему
                                        </Link>
                                        <button className="btn btn-danger" onClick={()=>deleteFunction(problem.title)}>Удалить проблему</button>
                                        <button className="btn btn-danger" onClick={()=>sendFunction(problem.title)}>Отправить экспертам</button>
                                    </div>
                                </div>
                            )
                        }
                    })}
            </div>
        </div>
    )
}
