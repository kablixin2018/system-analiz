import React, {useCallback, useEffect, useState, useContext,useRef} from 'react'
import {Link} from 'react-router-dom'
import {useHttp, } from '../../../hooks/http.hook'
import { useMessage } from '../../../hooks/message.hook'
import {AuthContext} from '../../../context/AuthContex'

export const FmpCardExp = ({problem,title,alt}) =>{
  const auth = useContext(AuthContext)
  const message = useMessage()
  const {loading,error,request,clearError}=useHttp()
  const [form,setForm]=useState({
      name:'',value:''
    })
  let forms = []
  useEffect(()=>{
    message(error)
    clearError()  
  },[error,message,clearError])

  const tmp_alt = alt
  const changeHandler = (event,second) =>{
      console.log("event.target.name",event.target.name)
      console.log("event.target.value",event.target.value)
      let input_item = {
          first_name: event.target.name,
          second_name: second.text,
          value: event.target.value
        }
        forms.push(input_item)
        console.log(forms)

  }

  const changeInputHandler = event =>{
    console.log("INPUT NAME",event.target.name)
    console.log("INPUT ID",event.target.id)
    console.log("INPUT VALUE",event.target.value)
    const name = event.target.name
    const id = event.target.id
    console.log(forms)
    console.log("FIRST second",name,id)
    const value = event.target.value
    for(let i=0; i<forms.length;i++){
      if(forms[i].first_name==name && forms[i].second_name == id){
        forms[i].size = value
      }
    }
  }

  const sendHandeler = async () =>{
    console.log(forms)
    
    const data_res = {
      userName: auth.userName,
      alts: alt,
      answer: forms
      }
    try {
        const data = await request('/api/result/fpm',"POST", data_res)
        message(data.message)
        window.location.reload()
      } catch (e) {}
    }
  
    return(
      <div className="container">
          <div className="row">
              <h1>{problem.title}</h1>
              <p>Сравните альтернативы. Для каждой пары выберите наиболее предпочтительную альтернативу, 
                  либо отметьте, что альтернативы однозначны.</p>
          </div>

          <div className="row">
              <form name="Fpm">
                  <table>
                      <tr className="tr" nowrap>
                        {alt.map(alt_current=>{
                            return(
                                <div className="col-md-12">
                                    {tmp_alt.map(tmp_alt_current=>{
                                        let parent = alt.indexOf(alt_current)
                                        let children =tmp_alt.indexOf(tmp_alt_current) 
                                        if(tmp_alt_current.text!=alt_current.text && parent<children)
                                            return(
                                                <div>
                                                    <th className="table_th"  widht="4px" nowrap>
                                                      <span>{alt_current.text}</span>
                                                    </th>
                                                    <th className="table_th" widht="4" nowrap>
                                                        <span>{tmp_alt_current.text}</span>

                                                    </th>
                                                    <th className="table_th th_check" align='left' bordercolor="#000" nowrap>
                                                        <select name={alt_current.text} onChange={(event)=>changeHandler(event,tmp_alt_current)}>
                                                            <option selected value="Крокодил Гена" disabled>Выберите из списка</option>
                                                            <option value = {alt_current.text}>Альтернатива 1</option>
                                                            <option value = {tmp_alt_current.text}>Альтернатива 2</option>
                                                            
                                                        </select> 
                                                    </th> 
                                                    <th className="table_th th_text" align='left' nowrap>
                                                      <span>предрочтительнее второй альтернативы </span>
                                                      <input name={alt_current.text} id={tmp_alt_current.text} size="1" onChange={changeInputHandler}></input>
                                                      <span> в случаях из {problem.value}</span>
                                                    </th>
                                            </div>
                                            )                                
                                    })}
                                </div>
                            )
                        })}
                      </tr>
              </table>
              </form>
          </div>
          <div className="form-group">
                    <button type="submit" className="btn btn-default" disabled={loading} onClick={sendHandeler}>Отправить</button>
              </div>
      </div>
  ) 
  
}